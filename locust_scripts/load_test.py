'''
This is a simple locust script to test performance characteristics
of the Resume Parser endpoint (/createProfile)
'''

from json import JSONDecodeError
import logging
from locust import task, constant
from locust.contrib.fasthttp import FastHttpUser


logger = logging.getLogger(__name__)


class LocustClient(FastHttpUser):
    '''
    running the script:
    locust -f load_test.py
    '''

    host = 'https://qa-resumeparser-api.jobtarget.com'
    wait_time = constant(0) #between(1, 5)

    def __init__(self, environment):
        ''' Class constructor '''
        super().__init__(environment)

    def on_start(self):
        ''' on_start is called when a Locust start before any task is scheduled '''
        pass

    def on_stop(self):
        ''' on_stop is called when the TaskSet is stopping '''
        pass

    @task
    def load_rest_api_based_service(self):
        '''
        Test and hammer /createProfile endpoint only.
        This does not test functionality of parsing resumes.
        '''

        try:

            # Define createProfile endpoint
            api_post_endpoint = '/api/createProfile'

            # Set static payload for createProfile endpoint
            api_payload = {
                            "resumeUrl": "https://jobseeker-app-uploads.s3.amazonaws.com/cv/27873191/a01aecd9-a856-4bcf-9c84-bba668d717df",
	                        "resumeId": 742,
	                        "jobId": 331300,
	                        "jobseekerId": 54321,
	                        "candidateId": 11123,
	                        "resumeGuid": "ce0f0a9f-86cd-422a-add6-5baec10088381234",
	                        "wait": False  # True - | False - no real-time response
                            }

            # Send post request to createProfile endpoint to send resume url
            with self.client.post(api_post_endpoint, json=api_payload, 
                catch_response=True, name = '/createProfile') as response:

                try:
                    # Verify response, extract transactionId if request is success
                    if response.json()["statusCode"] != 200:
                        response.failure(f"Error submitting resume: {response.json()}")
                        logger.error(response.json())
                    else:
                        response.success()

                except JSONDecodeError:
                    response.failure(f"Response could not be decoded as JSON: {response.json()}")
                    logger.error(response.json())
                except KeyError:
                    response.failure(f"Response did not contain expected key 'statusCode':{response.json()}")
                    logger.error(response.json())

        # Catch all other exceptions not defined
        except Exception as all_exceptions:
            logger.error("Exception occurred! details are %s", all_exceptions)
