'''
This script is not a locust file.
This testing will send a set number of requests in succession.
The time elapsed from sending the request until the parsed resume
is available in dynamoDB is recorded under 'response_time.csv'
'''

import time
import csv
from statistics import mean
import requests
from tools.dynamodb import DynamoDBConnection
  
# api-endpoint
URL = "https://qa-resumeparser-api.jobtarget.com"

# defining a params dict for the parameters to be sent to the API
PARAMS =   {
            "resumeUrl": "https://jobseeker-app-uploads.s3.amazonaws.com/cv/27873191/a01aecd9-a856-4bcf-9c84-bba668d717df",
            "resumeId": 742,
            "jobId": 332300,
            "jobseekerId": 54321,
            "candidateId": 11123,
            "resumeGuid": "ce0f0a9f-86cd-422a-add6-5baec10088381234",
            "wait": True
            }

# defining keys for dynamodb connection
AWS_ACCESS_KEY_ID = "AKIATKRKXLMBHWYYPRXK"
AWS_SECRET_ACCESS_KEY = "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"

# set number of iterations
ITER_REQUEST = 10

# initializing other variables
transactionId_ls = []
elapsed_ls = []
data_response = []

for i in range(0,ITER_REQUEST):

    print(f'\n----------- iteration {i+1} -----------\n')

    # connect to qa-affinda-parsed-profile table
    dynadb = DynamoDBConnection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)

    # sending post request and saving the response as response object
    r = requests.post(url = f'{URL}/api/createProfile', params = PARAMS)

    # record start time
    start_time = time.time()

    # extracting data in json format
    data = r.json()

    transactionId = data['transactionId']

    print(f'TRANSACTION ID:{transactionId}')

    # define dynamodb table to access
    TABLE = 'qa-affinda-parsed-profile'

    # define partition key to filter
    PARTN_KEY = 'id'

    # initialize variable to hold dynamodb response
    query_item = []

    # start querying dynamodb
    while len(query_item) == 0:
        query_item = dynadb._query_item(TABLE, PARTN_KEY, transactionId)

    print(f"Item: {query_item}")

    # record end time
    end_time = time.time()

    # record elapsed time
    elapsed_time = end_time - start_time

    elapsed_ls.append(elapsed_time)

    print(f'\nELAPSED TIME AFTER SUCCESS: {elapsed_time}\n')


elapsed_ave = mean(elapsed_ls)

log_msg = (f'\n-----------------------------------------\n\
        ITERATIONS : {i+1}\n\
        AVERAGE ELAPSED TIME : {elapsed_ave} seconds\n')

print(log_msg)

with open('response_time.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(zip(transactionId_ls, elapsed_ls, data_response))
