"""
Programmatic API testing on DynamoDB tables.
This module has been reused from jtqa automation.
"""

import boto3
from boto3.dynamodb.conditions import Attr, Key
from botocore.config import Config


class DynamoDBConnection:

    default_config = Config(
        region_name='us-east-1',
        signature_version='v4',
        retries = {
            'max_attempts': 10,
            'mode': 'standard'
            }
        )

    def __init__(self, aws_access_key_id, aws_secret_access_key):
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key

    def _open_connection(self, service="dynamodb"):
        if service == 'sts':
            client = boto3.client('sts',aws_access_key_id=self.aws_access_key_id,
                                aws_secret_access_key=self.aws_secret_access_key, 
                                config=self.default_config)
        else:
            client = boto3.resource('dynamodb',aws_access_key_id=self.aws_access_key_id,
                                aws_secret_access_key=self.aws_secret_access_key, 
                                config=self.default_config)
        return client

    def _list_table(self):
        dbclient = self._open_connection()
        response = list(dbclient.tables.all())
        print(response)


    def _get_identity(self):
        dbclient = self._open_connection('sts')
        response = dbclient.get_caller_identity()
        for key in response:
            print(key, ':', response[key])

    def _query_item(self, *args):
        dbclient = self._open_connection()
        table = dbclient.Table(args[0])
        response = table.query(
            KeyConditionExpression=Key(args[1]).eq(args[2])
        )
        # data = response['Items']
        # for key in data:
        #     print(key)
        return response['Items']

    def _query_item_filter(self, *args):
        dbclient = self._open_connection()
        table = dbclient.Table(args[0])
        response = table.scan(
            FilterExpression=Attr(args[1]).eq(args[2])
        )
        return response['Items']

    def _query_table_item(self, *args):
        dbclient = self._open_connection()
        table = dbclient.Table(args[0])
        response = table.scan()
        data = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])

        return data
